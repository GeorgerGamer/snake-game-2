// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerBasePawn.h"
#include "Engine/Classes/Camera/CameraComponent.h"
#include "SnakeBase.h"
#include "Food.h"
#include "Components/InputComponent.h"

// Sets default values
APlayerBasePawn::APlayerBasePawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	PawnCamera = CreateDefaultSubobject<UCameraComponent>("PawnCamera");
	RootComponent = PawnCamera;

}

// Called when the game starts or when spawned
void APlayerBasePawn::BeginPlay()
{
	Super::BeginPlay();
	SetActorRotation(FRotator(-90, 0, 0));
	CreateSnakeActor();

	SpawnFood();
	
}

// Called every frame
void APlayerBasePawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (FoodActor->bEaten == true)
	{
		SpawnFood();
	}

}

// Called to bind functionality to input
void APlayerBasePawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAxis("HorizontalMapping", this, &APlayerBasePawn::HandlePlayerHorizontalInput);
	PlayerInputComponent->BindAxis("VerticalMapping", this, &APlayerBasePawn::HandlePlayerVerticalInput);

}

void APlayerBasePawn::CreateSnakeActor()
{
	SnakeActor = GetWorld()->SpawnActor<ASnakeBase>(SnakeActorClass, FTransform());

}

void APlayerBasePawn::HandlePlayerHorizontalInput(float value)
{
	if (IsValid(SnakeActor))
	{
		if (value > 0 && SnakeActor->DeltaDirection != EMovementDirection::LEFT)
		{
			SnakeActor->LastMovementDirection = EMovementDirection::RIGHT;
		}

		if (value < 0 && SnakeActor->DeltaDirection != EMovementDirection::RIGHT)
		{
			SnakeActor->LastMovementDirection = EMovementDirection::LEFT;
		}
	}

}

void APlayerBasePawn::HandlePlayerVerticalInput(float value)
{
	if (IsValid(SnakeActor))
	{
		if (value > 0 && SnakeActor->DeltaDirection != EMovementDirection::DOWN)
		{
			SnakeActor->LastMovementDirection = EMovementDirection::UP;
		}

		if (value < 0 && SnakeActor->DeltaDirection != EMovementDirection::UP)
		{
			SnakeActor->LastMovementDirection = EMovementDirection::DOWN;
		}
	}

}

int APlayerBasePawn::Random(int min, int max)
{
	return min + rand() / (RAND_MAX / (max - min + 1) + 1);
	
}

void APlayerBasePawn::SpawnFood()
{
	FVector RandLocation(Random(-9, 9) * 100, Random(-9, 9) * 100, 6);
	FTransform RandTransform = FTransform(RandLocation);
	FoodActor = GetWorld()->SpawnActor <AFood>(FoodActorClass, RandTransform);
	FoodActor->bEaten = false;

}

