// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SNAKEGAME_PlayerBasePawn_generated_h
#error "PlayerBasePawn.generated.h already included, missing '#pragma once' in PlayerBasePawn.h"
#endif
#define SNAKEGAME_PlayerBasePawn_generated_h

#define SnakeGame_Source_SnakeGame_PlayerBasePawn_h_16_SPARSE_DATA
#define SnakeGame_Source_SnakeGame_PlayerBasePawn_h_16_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execRandom); \
	DECLARE_FUNCTION(execHandlePlayerVerticalInput); \
	DECLARE_FUNCTION(execHandlePlayerHorizontalInput);


#define SnakeGame_Source_SnakeGame_PlayerBasePawn_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execRandom); \
	DECLARE_FUNCTION(execHandlePlayerVerticalInput); \
	DECLARE_FUNCTION(execHandlePlayerHorizontalInput);


#define SnakeGame_Source_SnakeGame_PlayerBasePawn_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPlayerBasePawn(); \
	friend struct Z_Construct_UClass_APlayerBasePawn_Statics; \
public: \
	DECLARE_CLASS(APlayerBasePawn, APawn, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeGame"), NO_API) \
	DECLARE_SERIALIZER(APlayerBasePawn)


#define SnakeGame_Source_SnakeGame_PlayerBasePawn_h_16_INCLASS \
private: \
	static void StaticRegisterNativesAPlayerBasePawn(); \
	friend struct Z_Construct_UClass_APlayerBasePawn_Statics; \
public: \
	DECLARE_CLASS(APlayerBasePawn, APawn, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeGame"), NO_API) \
	DECLARE_SERIALIZER(APlayerBasePawn)


#define SnakeGame_Source_SnakeGame_PlayerBasePawn_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APlayerBasePawn(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APlayerBasePawn) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APlayerBasePawn); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APlayerBasePawn); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APlayerBasePawn(APlayerBasePawn&&); \
	NO_API APlayerBasePawn(const APlayerBasePawn&); \
public:


#define SnakeGame_Source_SnakeGame_PlayerBasePawn_h_16_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APlayerBasePawn(APlayerBasePawn&&); \
	NO_API APlayerBasePawn(const APlayerBasePawn&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APlayerBasePawn); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APlayerBasePawn); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APlayerBasePawn)


#define SnakeGame_Source_SnakeGame_PlayerBasePawn_h_16_PRIVATE_PROPERTY_OFFSET
#define SnakeGame_Source_SnakeGame_PlayerBasePawn_h_13_PROLOG
#define SnakeGame_Source_SnakeGame_PlayerBasePawn_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeGame_Source_SnakeGame_PlayerBasePawn_h_16_PRIVATE_PROPERTY_OFFSET \
	SnakeGame_Source_SnakeGame_PlayerBasePawn_h_16_SPARSE_DATA \
	SnakeGame_Source_SnakeGame_PlayerBasePawn_h_16_RPC_WRAPPERS \
	SnakeGame_Source_SnakeGame_PlayerBasePawn_h_16_INCLASS \
	SnakeGame_Source_SnakeGame_PlayerBasePawn_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SnakeGame_Source_SnakeGame_PlayerBasePawn_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeGame_Source_SnakeGame_PlayerBasePawn_h_16_PRIVATE_PROPERTY_OFFSET \
	SnakeGame_Source_SnakeGame_PlayerBasePawn_h_16_SPARSE_DATA \
	SnakeGame_Source_SnakeGame_PlayerBasePawn_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	SnakeGame_Source_SnakeGame_PlayerBasePawn_h_16_INCLASS_NO_PURE_DECLS \
	SnakeGame_Source_SnakeGame_PlayerBasePawn_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNAKEGAME_API UClass* StaticClass<class APlayerBasePawn>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SnakeGame_Source_SnakeGame_PlayerBasePawn_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
